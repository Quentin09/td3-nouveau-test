from box import *
# on veut pouvoir créer des boites
def test_box_create():
    b = Box()

def test_box_contenu():
    b = Box()
    b.add("machin")
    b.add("truc")
    assert "machin" in b
    assert "bidule" not in b
    assert "truc" in b

def test_box_enleve():
    b = Box()
    b.add("machin")
    b.add("truc")
    assert "machin" in b
    assert "bidule" not in b
    assert "truc" in b
    b.remove("machin")
    assert "machin" not in b
    assert "bidule" not in b
    assert "truc" in b

def test_box_is_open():
    b = Box()
    assert b.is_open()

def test_b_close():
    b = Box()
    b.close()
    assert not b.is_open()

def test_b_open():
    b = Box()
    b.open()
    assert b.is_open()

def test_b_action_look():
    b = Box()
    b.add("truc")
    b.add("machin")
    assert b.action_look() == "la boite contient: truc, machin"
    b.close()
    assert b.action_look() == "la boite est fermee"
  
    
from thing import *

def test_thing_creation():
    t = Thing(3,"chose")

def test_thing_volume():
    t = Thing(3,"chose")
    assert t.volume() == 3

def test_b_set_capacity():
    b = Box()
    b.set_capacity(5)
    assert b.capacity() == 5
    c = Box()
    assert c.capacity() == None

def test_b_has_room_for():
    b = Box()
    b.set_capacity(6)
    t = Thing(3,"chose")
    assert b.has_room_for(t)
    c = Box()
    assert c.has_room_for(t)

def test_b_action_add():
    b = Box()
    b.set_capacity(10)
    b.open()
    t = Thing(3,"chose")
    b.action_add(t)
    t1 = Thing(11,'machin')
    b.action_add(t1)
    assert t in b
    assert t1 not in b

def test_thing_set_name():
    t = Thing(3,"chose")
    t.set_name("vélo")
    assert t.has_name("vélo")

def test_b_find():
    b = Box()
    b.set_capacity(10)
    b.open()
    t = Thing(3,"chose")
    b.action_add(t)
    assert b.find("chose") == t
    assert b.find("quoi") == None